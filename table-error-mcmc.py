#!/usr/bin/env python3
##################################################
# Create TeX-table from mcmc samples with mean
# and deviation
#
# @param filename
##################################################
import sys

import os.path

import h5py
import numpy as np
import scipy.special
import scipy.stats

import libeos
import liberror

filename = sys.argv[1]

f = h5py.File(filename, 'r')

parameters = libeos.parse_parameters(f)
paths = sorted([('/main run/{}/samples'.format(c), None) for c in f['main run']])

f.close() # important!
params = [p for p in parameters.values() if not p.nuisance]

results = libeos.histogram_chunked(filename, paths, [[p] for p in params])
xbounds = []
for (hist, (bins,), _), p in zip(results, params):
    ybound = libeos.region_heights(hist, [1])
    ybound_diff = np.ediff1d((hist > ybound).astype(int))
    bounds = np.where(ybound_diff != 0)[0]
    if len(bounds) % 2 != 0:
        if ybound_diff[bounds[0]] == -1:
            bounds = np.hstack((bounds, [0]))
        else:
            bounds = np.hstack((bounds, [len(hist) - 1]))

    mode_bounds = np.split(bounds, len(bounds) / 2)
    sums = [np.sum(hist[x1:x2+1]) for x1, x2 in mode_bounds]
    mode_index = np.argmax(sums)
    xbounds.append([(bins[x1], bins[x2+1]) for x1, x2 in mode_bounds][mode_index])

xbounds = np.array(xbounds)
means = (xbounds[:,1] + xbounds[:,0]) / 2
variances = (xbounds[:,1] - xbounds[:,0]) / 2

options = {
    "ee->PSI(3770)::width": {
        'title': r"$Γ^{r → \APelectron\Pelectron}(\Ppsi(3770)) \mathbin{/} \si{\kilo \electronvolt}$",
        'experimental': {
            'mean': [0.22],
            'stdev': [0.05],
        },
        'prefactor': 1e6,
    },
    "ee->PSI(4040)::width": {
        'title': r"$Γ^{r → \APelectron\Pelectron}(\Ppsi(4040)) \mathbin{/} \si{\kilo \electronvolt}$",
        'experimental': {
            'mean': [0.83],
            'stdev': [0.20],
        },
        'prefactor': 1e6,
    },
    "ee->PSI(4160)::width": {
        'title': r"$Γ^{r → \APelectron\Pelectron}(\Ppsi(4160)) \mathbin{/} \si{\kilo \electronvolt}$",
        'experimental': {
            'mean': [0.48],
            'stdev': [0.22],
        },
        'prefactor': 1e6,
    },
    "ee->PSI(4415)::width": {
        'title': r"$Γ^{r → \APelectron\Pelectron}(\Ppsi(4415)) \mathbin{/} \si{\kilo \electronvolt}$",
        'experimental': {
            'mean': [0.35],
            'stdev': [0.12],
        },
        'prefactor': 1e6,
    },
    "ee->hadrons::a_cont": {
        'title': r"$a_{\text{cont}}$",
    },
    "ee->hadrons::fit_width(3770)": {
        'title': r"$\hat Γ_{r}(\Ppsi(3770)) \mathbin{/} \si{\mega \electronvolt}$",
        'prefactor': 1e3,
    },
    "ee->hadrons::fit_width(4040)": {
        'title': r"$\hat Γ_{r}(\Ppsi(4040)) \mathbin{/} \si{\mega \electronvolt}$",
        'prefactor': 1e3,
    },
    "ee->hadrons::fit_width(4160)": {
        'title': r"$\hat Γ_{r}(\Ppsi(4160)) \mathbin{/} \si{\mega \electronvolt}$",
        'prefactor': 1e3,
    },
    "ee->hadrons::fit_width(4415)": {
        'title': r"$\hat Γ_{r}(\Ppsi(4415)) \mathbin{/} \si{\mega \electronvolt}$",
        'prefactor': 1e3,
    },
    "ee->hadrons::phase(4040)": {
        'title': r"$δ(\Ppsi(4040)) \mathbin{/} \si{\degree}$",
        'experimental': {
            'mean': [133],
            'stdev': [68],
        },
        'prefactor': 360 / np.pi,
        'phase': True,
    },
    "ee->hadrons::phase(4160)": {
        'title': r"$δ(\Ppsi(4160)) \mathbin{/} \si{\degree}$",
        'experimental': {
            'mean': [301],
            'stdev': [61],
        },
        'prefactor': 360 / np.pi,
        'phase': True,
    },
    "ee->hadrons::phase(4415)": {
        'title': r"$δ(\Ppsi(4415)) \mathbin{/} \si{\degree}$",
        'experimental': {
            'mean': [246],
            'stdev': [86],
        },
        'prefactor': 360 / np.pi,
        'phase': True,
    },
    "mass::PSI(3770)": {
        'title': r"$m(\Ppsi(3770)) \mathbin{/} \si{\mega \electronvolt}$",
        'experimental': {
            'mean': [3771.4],
            'stdev': [1.8],
        },
        'prefactor': 1e3,
    },
    "mass::PSI(4040)": {
        'title': r"$m(\Ppsi(4040)) \mathbin{/} \si{\mega \electronvolt}$",
        'experimental': {
            'mean': [4038.5],
            'stdev': [4.6],
        },
        'prefactor': 1e3,
    },
    "mass::PSI(4160)": {
        'title': r"$m(\Ppsi(4160)) \mathbin{/} \si{\mega \electronvolt} $",
        'experimental': {
            'mean': [4191.6],
            'stdev': [6.0],
        },
        'prefactor': 1e3,
    },
    "mass::PSI(4415)": {
        'title': r"$m(\Ppsi(4415)) \mathbin{/} \si{\mega \electronvolt}$",
        'experimental': {
            'mean': [4415.2],
            'stdev': [7.5],
        },
        'prefactor': 1e3,
    },
}

# toggle between experimental values
toggle = 0

with open("{}/errors-{}.tex".format(os.path.dirname(filename), os.path.splitext(os.path.basename(filename))[0].rsplit('.')[0]), "w") as f:
    for parameter, m, v in sorted(zip(params, means, variances), key=lambda x: x[0].id):
        m_print = m * options[parameter.id].get('prefactor', 1)
        if 'experimental' in options[parameter['name']]:
            f.write("{} & {:.2f} \pm {:.2f} & {:.2f} \pm {:.2f} \\\\ \n".format(options[parameter.id]['title'], np.remainder(m_print, 360) if options[parameter.id].get('phase', False) else m_print, v * options[parameter.id].get('prefactor', 1), options[parameter['name']]['experimental']['mean'][toggle], options[parameter['name']]['experimental']['stdev'][toggle]))
            toggle += 1
            toggle = toggle % 1
        else:
            toggle = 0
            f.write("{} & {:.2f} \pm {:.2f} & \\\\ \n".format(options[parameter.id]['title'], np.remainder(m_print, 360) if options[parameter.id].get('phase', False) else m_print, v * options[parameter.id].get('prefactor', 1)))
