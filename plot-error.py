#!/usr/bin/env python3
##################################################
# Plot EOS observables with bayesian errorbands.
#
# @param filename
# @param observable
# @param integrated (true|True)
##################################################
import sys

import matplotlib as mpl
mpl.use("pdf")
import matplotlib.pyplot as plt
import numpy as np
import scipy.ndimage

from liberror import load_observables, load_integrated_observables, hist_errors, hist_integrated_errors, h
from libplot import options

if len(sys.argv) == 1:
    for opt in options:
        print(opt)
    exit()

name = sys.argv[2]
filename = '{}/errors-{}.pdf'.format(sys.argv[1].rsplit('/', 1)[0], name.replace('/', ''))
image_filename = '{}/errors-{}.png'.format(sys.argv[1].rsplit('/', 1)[0], name.replace('/', ''))
integrated = False
png = False
combine = False

if len(sys.argv) >= 4:
    if sys.argv[3] == "true" or sys.argv[3] == "True":
        integrated = True

if len(sys.argv) >= 5:
    if sys.argv[4] == "--png":
        png = True
    elif sys.argv[4] == "--combine":
        combine = True

if not name in options:
    options = {
        name: {
            'title': r"$"+name+"$",
            'xlim': [10, 19.0],
            'ylim': [1,-1],
            'xlabel': r"$q^2 \mathbin{/} \si{GeV}$",
            'ylabel': ""
        }
    }

if not png:
    fig, ax = plt.subplots(1, 1, figsize=options[name].get('figsize', (5, 3.112)))
else:
    fig = plt.figure(figsize=options[name].get('figsize', (5, 3.112)))
    ax = fig.add_axes([0, 0, 1, 1])

if integrated:
    x_min, x_max, y = load_integrated_observables(sys.argv[1], name)
else:
    x, y =  load_observables(sys.argv[1], name, {"one-loop": "false"})

##                     ##
## Observable specific ##
##                     ##
if name == 'ee->hadrons::charm_loop_img' or name == 'ee->hadrons::charm_loop_real' or name == 'B->Kll::dBR/ds@LowRecoil':
    x = np.sqrt(x)

if name == 'B->Kll::dBR/ds@LowRecoil':
    for i in range(len(x)):
        y[i] = 2.0 * x[i] * y[i]

if not combine:
    if integrated:
        hist_integrated_errors(x_min, x_max, y, ax)
    else:
        hist_errors(x, y, ax)

#
#
#

# ax.set_title(options[name]['title'])
ax.set_xlim(options[name]['xlim'][0], options[name]['xlim'][1])
ax.set_ylim(options[name]['ylim'][0], options[name]['ylim'][1])

if combine:
    image = scipy.ndimage.imread(image_filename)
    ax.imshow(image, interpolation='none', origin='upper', extent=(options[name]['xlim'][0], options[name]['xlim'][1], options[name]['ylim'][0], options[name]['ylim'][1]), aspect='auto')

if not png:
    ax.grid()

    ax.set_xlabel(options[name]['xlabel'])
    ax.set_ylabel(options[name]['ylabel'])
else:
    ax.set_aspect('auto', adjustable='box')

#
#
#
if not png:
    if name == 'ee->hadrons::ratio':
        q, r, err = np.loadtxt("data/BESII_2007_ratio.dat", unpack=True)
        ax.errorbar(q, r, yerr=err, fmt='k_', ecolor='darkslategrey')


    if name == 'ee->hadrons::charm_loop_img' or name == 'ee->hadrons::charm_loop_real':
        y_one_loop = h(4.2, x * x)
        if name.rsplit('_', 1)[1] == 'real' :
            y_one_loop = y_one_loop.real
        elif name.rsplit('_', 1)[1] == 'img':
            y_one_loop = y_one_loop.imag

        ax.plot(x, y_one_loop, '--k')


    if name == 'B->Kll::dBR/ds@LowRecoil' or name == 'B->Kll::dBR/ds@LowRecoil':
        x1, x2, br, yerr = np.loadtxt("data/highq2_BF.dat", unpack=True)
        x1 = x1 * 1.0e-3
        x2 = x2 * 1.0e-3
        x = (x2 + x1) / 2.0
        xerr = (x2 - x1) / 2.0
        ax.errorbar(x, br, xerr=xerr, yerr=yerr, fmt='k_', ecolor='darkslategrey')
        x_bsm, y_bsm = load_observables('results/ratio-ee-to-hadrons-BSMI/propagate.B->Kll::dBRds@LowRecoil.hdf5', name)
        x_bsm = np.sqrt(x_bsm)
        for i in range(len(x_bsm)):
            y_bsm[i] = 2 * x_bsm[i] * y_bsm[i]
        ax.plot(x_bsm, np.mean(y_bsm, axis=-1), '--g')
        x_bsm, y_bsm = load_observables('results/ratio-ee-to-hadrons-BSMII/propagate.B->Kll::dBRds@LowRecoil.hdf5', name)
        x_bsm = np.sqrt(x_bsm)
        for i in range(len(x_bsm)):
            y_bsm[i] = 2 * x_bsm[i] * y_bsm[i]
        ax.plot(x_bsm, np.mean(y_bsm, axis=-1), ':r')
        # x_bsm, y_bsm = np.loadtxt("results/b-to-kll-np/evaluate.B->Kll::dBRds@LowRecoil.dat", unpack=True)
        # x_bsm = np.sqrt(x_bsm)
        # y_bsm = 2 * x_bsm * y_bsm
        # ax.plot(x_bsm, y_bsm, '--g')

    if name == 'B->K^*ll::dBR/ds@LowRecoil':
        s, Δs, y, err = np.loadtxt("data/b-to-kstar-dBRds-CMS.dat", unpack=True, skiprows=1)
        ax.errorbar(s, y, xerr=Δs, yerr=err, fmt='gx', label="CMS")
        ax.legend(loc="upper right", numpoints=1)

    if name == 'B->K^*ll::F_L(s)@LowRecoil':
        s, Δs, y, err = np.loadtxt("data/b-to-kstar-F_L.dat", unpack=True, skiprows=1)
        ax.errorbar(s, y, xerr=Δs, yerr=err, fmt='r_', label="LHCb")
        s, Δs, y, err = np.loadtxt("data/b-to-kstar-F_L-CMS.dat", unpack=True, skiprows=1)
        ax.errorbar(s, y, xerr=Δs, yerr=err, fmt='gx', label="CMS")
        ax.legend(loc="upper right", numpoints=1)

    if name == 'B->K^*ll::A_FB(s)@LowRecoil':
        s, Δs, y, err = np.loadtxt("data/b-to-kstar-A_FB.dat", unpack=True, skiprows=1)
        y = -y                  #  Sign Convention ???
        ax.errorbar(s, y, xerr=Δs, yerr=err, fmt='r_', label="LHCb")
        s, Δs, y, err = np.loadtxt("data/b-to-kstar-A_FB-CMS.dat", unpack=True, skiprows=1)
        y = -y                  #  Sign Convention ???
        ax.errorbar(s, y, xerr=Δs, yerr=err, fmt='gx', label="CMS")
        ax.legend(loc="upper right", numpoints=1)


    if name == 'B->K^*ll::F_Lavg@LowRecoil':
        s, Δs, y, err = np.loadtxt("data/b-to-kstar-F_L.dat", unpack=True, skiprows=1)
        ax.errorbar(s, y, xerr=Δs, yerr=err, fmt='k_', ecolor='darkslategrey')

    if name == 'B->K^*ll::A_FBavg@LowRecoil':
        s, Δs, y, err = np.loadtxt("data/b-to-kstar-A_FB.dat", unpack=True, skiprows=1)
        y = -y                  #  Sign Convention ???
        ax.errorbar(s, y, xerr=Δs, yerr=err, fmt='k_', ecolor='darkslategrey')

    if name == 'B->K^*ll::J_3normavg@LowRecoil':
        s, Δs, y, err = np.loadtxt("data/b-to-kstar-S_3.dat", unpack=True, skiprows=1)
        y = 3 / 4 * y
        err = 3 / 4 * err
        ax.errorbar(s, y, xerr=Δs, yerr=err, fmt='k_', ecolor='darkslategrey')

    if name == 'B->K^*ll::J_4normavg@LowRecoil':
        s, Δs, y, err = np.loadtxt("data/b-to-kstar-S_4.dat", unpack=True, skiprows=1)
        y = 3 / 4 * y
        err = 3 / 4 * err
        ax.errorbar(s, y, xerr=Δs, yerr=err, fmt='k_', ecolor='darkslategrey')

    if name == 'B->K^*ll::J_5normavg@LowRecoil':
        s, Δs, y, err = np.loadtxt("data/b-to-kstar-S_5.dat", unpack=True, skiprows=1)
        y = 3 / 4 * y
        err = 3 / 4 * err
        ax.errorbar(s, y, xerr=Δs, yerr=err, fmt='k_', ecolor='darkslategrey')

    if name == "B->K^*ll::P'_5@LowRecoil":
        s, Δs, y, err = np.loadtxt("data/b-to-kstar-p5-prime.dat", unpack=True, skiprows=1)
        ax.errorbar(s, y, xerr=Δs, yerr=err, fmt='k_', ecolor='darkslategrey')

#
#
if not png:
    fig.tight_layout()
    fig.savefig(filename)
else:
    ax.set_axis_off()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)

    fig.savefig(filename, bbox_inches='tight', pad_inches=0)

plt.close(fig)
