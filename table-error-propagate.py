#!/usr/bin/env python3
##################################################
# Create TeX-table from propagated mcmc samples
# with mean and deviation
#
# @param filename
##################################################
import sys

import os.path

import h5py
import numpy as np
import scipy.special
import scipy.stats

import libeos
import liberror

if len(sys.argv) == 1:
    for opt in options:
        print(opt)
    exit()

filename = sys.argv[1]

f = h5py.File(filename, 'r')

observables = libeos.parse_observables(f)
obs = [o for o in observables.values()]
paths = [('data/parameters' if 'data/parameters' in f else None,
         'data/observables' if 'data/observables' in f else None, )]

f.close()

results = libeos.histogram_chunked(filename, paths, [[o] for o in obs])
xbounds = []
for (hist, (bins,), _), p in zip(results, obs):
    ybound = libeos.region_heights(hist, [1])
    ybound_diff = np.ediff1d((hist > ybound).astype(int))
    bounds = np.where(ybound_diff != 0)[0]
    if len(bounds) % 2 != 0:
        if ybound_diff[bounds[0]] == -1:
            bounds = np.hstack((bounds, [0]))
        else:
            bounds = np.hstack((bounds, [len(hist) - 1]))

    mode_bounds = np.split(bounds, len(bounds) / 2)
    sums = [np.sum(hist[x1:x2+1]) for x1, x2 in mode_bounds]
    mode_index = np.argmax(sums)
    xbounds.append([(bins[x1], bins[x2+1]) for x1, x2 in mode_bounds][mode_index])

xbounds = np.array(xbounds)
means = (xbounds[:,1] + xbounds[:,0]) / 2
variances = (xbounds[:,1] - xbounds[:,0]) / 2

options = {
    'ee->hadrons::total_width': {
        'title': {
            '1': r'$Γ_{r}(\Ppsi (3770)) \mathbin{/} \si{\mega \electronvolt}$',
            '2': r'$Γ_{r}(\Ppsi (4040)) \mathbin{/} \si{\mega \electronvolt}$',
            '3': r'$Γ_{r}(\Ppsi (4160)) \mathbin{/} \si{\mega \electronvolt}$',
            '4': r'$Γ_{r}(\Ppsi (4415)) \mathbin{/} \si{\mega \electronvolt}$',
        },
        'experimental': {
            'mean': [25.4, 81.2, 72.7, 73.3],
            'stdev': [6.5, 14.4, 15.1, 21.2],
        },
        'prefactor': {
            '1': 1e3,
            '2': 1e3,
            '3': 1e3,
            '4': 1e3,
        },
    },
    'B->Kll::BR@LowRecoil': {
        'title': r'$\mathcal{B} (\PB → \PK \Pmuon \APmuon)^{23.07}_{9.0} \cdot \num{10e-7}$',
        'prefactor': 1e7,
    },
    'B->Kll::dBR/ds@LowRecoil': {
        'title': r'$\frac{\mathup{d}\mathcal{B}}{\mathup{d}s}$',
        'prefactor': 1,
    },
    'B->Kll::BRavg@LowRecoil': {
        'title': r'$\langle \mathcal{B} \rangle$',
        'prefactor': 1,
    },
    "B->K^*ll::F_Lavg@LowRecoil": {
        'title': r'$F_{\text{L}}$',
        'prefactor': 1,
        'experimental': {
            'bin': ['11.0,12.5', '15.0,19.0', '15.0,17.0', '17.0,19.0'],
            'mean': [0.4930631, 0.3443552, 0.349, 0.354],
            'stdev': [0.04671594, 0.02982242, 0.041, 0.054],
        },
    },
    "B->K^*ll::A_FBavg@LowRecoil": {
        'title': r'$A_{\text{FB}}$',
        'prefactor': -1,
        'experimental': {
            'bin': ['11.0,12.5', '15.0,19.0', '15.0,17.0', '17.0,19.0'],
            'mean': [0.3175248, 0.3548299, 0.411, 0.354],
            'stdev': [0.04092561, 0.02956008, 0.041, 0.050 ],
        },
    },
    "B->K^*ll::J_3normavg@LowRecoil": {
        'title': r'$S_{3}$',
        'prefactor': 4/3,
        'experimental': {
            'bin': ['11.0,12.5', '15.0,19.0', '15.0,17.0', '17.0,19.0'],
            'mean': [-0.1888138, -0.162855, -0.142, -0.188],
            'stdev': [0.05582518, 0.03914936, 0.048, 0.088],
        },
    },
    "B->K^*ll::J_4normavg@LowRecoil": {
        'title': r'$S_{4}$',
        'prefactor': -4/3,
        'experimental': {
            'bin': ['11.0,12.5', '15.0,19.0', '15.0,17.0', '17.0,19.0'],
            'mean': [-0.2834745, -0.2835983, -0.321, -0.266],
            'stdev': [0.0923255, 0.04049261, 0.078, 0.072],
        },
     },
    "B->K^*ll::J_5normavg@LowRecoil": {
        'title': r'$S_{5}$',
        'prefactor': 4/3,
        'experimental': {
            'bin': ['11.0,12.5', '15.0,19.0', '15.0,17.0', '17.0,19.0'],
            'mean': [-0.3175248, -0.325, -0.316, -0.323],
            'stdev': [0.04092561, 0.039, 0.059, 0.070],
        },
    },
    "B->K^*ll::J_7normavg@LowRecoil": {
        'title': r'$S_{7}$',
        'prefactor': 4/3,
        'experimental': {
            'bin': ['11.0,12.5', '15.0,19.0', '15.0,17.0', '17.0,19.0'],
            'mean': [-0.1408857, 0.04806258, 0.061, 0.044],
            'stdev': [0.07493397, 0.04435568, 0.059, 0.074],
        },
    },
    "B->K^*ll::J_8normavg@LowRecoil": {
        'title': r'$S_{8}$',
        'prefactor': 4/3,
        'experimental': {
            'bin': ['11.0,12.5', '15.0,19.0', '15.0,17.0', '17.0,19.0'],
            'mean': [-0.007403174, 0.02805555, 0.003, 0.013],
            'stdev': [0.07259683, 0.04500867, 0.06, 0.071],
        },
    },
    "B->K^*ll::J_9normavg@LowRecoil": {
        'title': r'$S_{9}$',
        'prefactor': 4/3,
        'experimental': {
            'bin': ['11.0,12.5', '15.0,19.0', '15.0,17.0', '17.0,19.0'],
            'mean': [-0.003743301, -0.05322698, -0.019, -0.094],
            'stdev': [0.07317978, 0.0397787, 0.057, 0.069],
        },
    },
    "B->K^*ll::P'_5@LowRecoil": {
        'title': r"$P_{5}'$",
        'prefactor': 1,
        'experimental': {
            'bin': ['11.0,12.5', '15.0,19.0', '15.0,17.0', '17.0,19.0'],
            'mean': [-0.6544757, -0.6840621, -0.662, -0.675],
            'stdev': [0.141594, 0.08243373, 0.127, 0.153],
        },
    },
    "B->K^*ll::H_T^1@LowRecoil": {
        'title': r"$H_T^1$",
        'prefactor': 1,
        'experimental': {
            'bin': ['14.0,19.2', '15.0,19.0'],
            'mean': [0.997],
            'stdev': [0.002],
        },
    },
    "B->K^*ll::H_T^2@LowRecoil": {
        'title': r"$H_T^2$",
        'prefactor': 1,
        'experimental': {
            'bin': ['14.0,19.2', '15.0,19.0'],
            'mean': [-0.972],
            'stdev': [0.010],
        },
    },
    "B->K^*ll::H_T^3@LowRecoil": {
        'title': r"$H_T^3$",
        'prefactor': 1,
        'experimental': {
            'bin': ['14.0,19.2'],
            'mean': [-0.958],
            'stdev': [0.030],
        },
    },
}

# toggle between experimental values
toggle = 0

qsort = [(11.0, 12.5), (15.0, 19.0), (15.0,17.0), (17.0,19.0), (14.0,19.2), (0, 0)]

with open("{}/errors-{}.tex".format(os.path.dirname(filename), os.path.splitext(os.path.basename(filename))[0].rsplit('.')[1]),"w") as f:
    for parameter, m, v in sorted(zip(obs, means, variances), key=lambda x: (x[0].name, qsort.index((x[0].kinematics.get('s_min', 0), x[0].kinematics.get('s_max', 0))))):
        pname = parameter['name']
        # Awkward exception for Γ_r
        if pname == 'ee->hadrons::total_width' :
            if 'experimental' in options[pname]:
                pre = options[pname]['prefactor'][parameter['options']['total-width-res']]
                index = int(parameter['options']['total-width-res']) - 1

                f.write("{:s} & {:.2f} \pm {:.2f} & {:.2f} \pm {:.2f} \\\\ \n".format(options[pname]['title'][parameter['options']['total-width-res']], m * pre, v * abs(pre), options[pname]['experimental']['mean'][index], options[pname]['experimental']['stdev'][index]))
            else:
                pre = options[pname]['prefactor'][parameter['options']['total-width-res']]
                f.write("{:s} & {:.3f} \pm {:.3f} \\\\ \n".format(options[pname]['title'][parameter['options']['total-width-res']], m * pre, v * abs(pre)))
        else:
            pre = options[pname].get('prefactor', 1)
            s_min = parameter['kinematics']['s_min']
            s_max = parameter['kinematics']['s_max']
            qbin = options[pname]['experimental']['bin'].index("{},{}".format(s_min, s_max))
            mean_exp = options[pname]['experimental']['mean'][qbin]
            stdev_exp = options[pname]['experimental']['stdev'][qbin]

            if 'experimental' in options[pname]:
                f.write("$[{},{}]$ & {:s} & {:.3f} \pm {:.3f} & {:.3f} \pm {:.3f} \\\\ \n".format(s_min, s_max, options[pname]['title'], m * pre, v * abs(pre), mean_exp, stdev_exp))
            else:
                f.write("$[{},{}]$ & {:s} & {:.3f} \pm {:.3f} &  \\\\ \n".format(s_min, s_max, options[pname]['title'], m * pre, v * abs(pre)))
