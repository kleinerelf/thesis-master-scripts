import h5py
import matplotlib as mpl
import numpy as np

import libeos

def load_observables(filename, name, options={}):
    f = h5py.File(filename, "r")
    d = f['descriptions']['observables']

    observables = libeos.parse_observables(f)

    x = []
    indices = []

    for obs in observables.values():
        if obs['name'] == name and all(obs.options.get(k, None) == v for k, v in options.items()):
            x.append(list(obs['kinematics'].values())[0])
            indices.append(obs['index'])

    y = f['data']['observables'][...].T
    y = y[indices]

    x = np.array(x)
    i = x.argsort()
    x = x[i]
    y = y[i]

    return x, y

def load_integrated_observables(filename, name):
    f = h5py.File(filename, "r")
    d = f['descriptions']['observables']

    observables = libeos.parse_observables(f)

    x_min = []
    x_max = []
    indices = []

    for obs in observables.values():
        if obs['name'] == name:
            x_min.append(list(obs['kinematics'].values())[0])
            x_max.append(list(obs['kinematics'].values())[1])
            indices.append(obs['index'])

    y = f['data']['observables'][...].T
    y = y[indices]

    x_min = np.array(x_min)
    x_max = np.array(x_max)
    i = x_min.argsort()
    x_min = x_min[i]
    x_max = x_max[i]
    y = y[i]

    return x_min, x_max, y

def hist_errors(x, y, ax):
    x_width = x[1] - x[0]

    for i in range(len(x)):
        y_hist = np.histogram(y[i], bins=100)

        cmap = mpl.colors.ListedColormap(['w', 'lightskyblue', 'royalblue'])
        norm = libeos.region_norm(y_hist[0], [1, 2])

        ax.pcolor(np.array([x[i] - x_width / 2, x[i] + x_width / 2]), y_hist[1], np.atleast_2d(y_hist[0]).T, cmap=cmap, norm=norm, vmin=1)

def hist_integrated_errors(x_min, x_max, y, ax):
    for i in range(len(x_max)):
        x_width = x_max[i] - x_min[i]
        y_hist = np.histogram(y[i], bins=50)
        bin_sigma = libeos.region_heights(y_hist[0], [1, 2])
        for j in range(len(y_hist[0])):
            if y_hist[0][j] >= bin_sigma[1]:
                if y_hist[0][j] >= bin_sigma[0]:
                    colour = "royalblue"
                else:
                    colour = "lightskyblue"
                ax.add_patch(mpl.patches.Rectangle((x_min[i], y_hist[1][j]), x_width, y_hist[1][j+1] - y_hist[1][j], fc=colour, ec='none'))

##################################################

def work_mean(d):
    return [np.mean(d, axis=-1), d.shape[1]]

def work_var(d):
    return [np.var(d, axis=-1), d.shape[1]]

def mean_chunked(filename, paths, pars, chunk_size=int(1e6)):
    means = libeos.map_chunked(filename, paths, [pars], chunk_size, work_mean, [[]] * len(pars))

    means = [m[0] for m in means]
    lens = np.array([m[1] for m in means])
    means = [m[0] for m in means]

    means = np.vstack(means) * np.atleast_2d(lens).T
    means = np.sum(means, axis=0) / np.sum(lens)
    return means

def var_chunked(filename, paths, pars, chunk_size=int(1e6)):
    variances = libeos.map_chunked(filename, paths, [pars], chunk_size, work_var, [[]] * len(pars))

    variances = [v[0] for v in variances]
    lens = np.array([v[1] for v in variances])
    variances = [v[0] for v in variances]

    variances = np.vstack(variances) * np.atleast_2d(lens).T
    variances = np.sum(variances, axis=0) / np.sum(lens) # Taking the mean of th absolut squared differences
    return variances
#,----
#| Lowest order charm loop function
#`----

def h(mu, s):
 return 8 / 27 + 4 / 9 * (np.log(mu * mu / s) + 1j * np.pi)
