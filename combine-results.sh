#!/bin/bash

if [ $# -lt 2 ]; then
    echo "Usage - [FIRST-DIR] [SECOND-DIR]"
    echo "Matches same-named files line-by-line with paste."
fi

firstdir=${1%/}
seconddir=${2%/}

shopt -s nullglob
for file in ${firstdir}/errors-B-\>K\^\*ll::*.tex
do
    combined="final/$(basename "${file}")"
    ( set -x
        paste --delimiters='&' <(cut --delimiter='&' --fields=1-3 "${file}") <(cut --delimiter='&' --fields=3- "${seconddir}/$(basename "${file}")") > "${combined}"
    )
done
shopt -u nullglob
