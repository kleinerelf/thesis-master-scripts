#!/bin/bash

if [ $# -lt 1 ]; then
    echo "Usage - $0 mode"
    exit
fi

# $1: path to pdf
# $2: log (true|false)
# $3: type (pdf)
function output-path () {
    echo "${dir}/errors-${1}.${3:-pdf}$("${2:-false}" && echo ".log")"
}

# $1: extra subdir (true|false)
function final-path () {
    local path="final/$("${2:-true}" && echo "$(basename ${dir})")"
    mkdir -p "${path}"
    echo "${path}"
}

# $1: Path to propagate
# $2: Observable
# $3: Integrated (true|false)
function plot-single () {
    local args=(
	"${1}" # Path
	"${2}" # Observable
	"${3:-false}" # Integrated (True|False)
	"${4:-}" # --combine|--png
    )

    ( set -x
	"scripts/plot-error.py" ${args[@]}
    ) 2>&1 | tee "$(output-path ${2////} true pdf)"
}

function plot-multi () {
    for (( j=0; j<${#ObsList[@]}; j++ )); do
	i=${ObsList[$j]}
	if [ -z ${Integrated} ]; then
	    local BoolIntegrated=${Integrated}
	else
	    local BoolIntegrated=${ObsListIntegrated[$j]:-false}
	fi

	local path="${dir}/propagate.${i////}.hdf5"
	if [ -f "${path}" ]; then
	    "plot-single" ${path} ${i} ${Integrated}
	    "mv" $(output-path ${i////}) "$(final-path true)"
	    "mv" $(output-path ${i////} true) "$(final-path true)"
	else
	    echo "${path} not found"
	fi
    done
}

function plot-combined () {
    for (( j=0; j<${#ObsList[@]}; j++ )); do
	i=${ObsList[$j]}
	if [[ ! -z ${Integrated} ]]; then
	    local BoolIntegrated=${Integrated}
	else
	    local BoolIntegrated=${ObsListIntegrated[$j]:-false}
	fi

	local path="${dir}/propagate.${i////}.hdf5"
	if [ -f "${path}" ]; then
	    "plot-single" ${path} ${i} ${BoolIntegrated} "--png"
	    "pdftoppm" -png -singlefile -r 1000 $(output-path ${i////}) ${dir}/errors-${i////}
	    "plot-single" ${path} ${i} ${BoolIntegrated} "--combine"
	    "mv" $(output-path ${i////}) "$(final-path true)"
	    "mv" $(output-path ${i////} true) "$(final-path true)"
	else
	    echo "${path} not found"
	fi
    done
}

function table-error-mcmc () {
    local path="${dir}/mcmc.hdf5"

    ( set -x
      "scripts/table-error-mcmc.py" "${path}"
    ) 2>&1 | tee "$(output-path "mcmc" true tex)"
    mv $(output-path "mcmc" false "tex") "$(final-path true)"
    mv $(output-path "mcmc" true "tex") "$(final-path true)"
}

function table-error-propagate () {
    local args=(
	"${1}" # Path
    )

    for (( j=0; j<${#ObsList[@]}; j++ )); do
	i=${ObsList[$j]}
	local BoolIntegrated=${ObsListIntegrated[$j]:-false}

	if [ $BoolIntegrated == true ] ; then
	    local path="${dir}/propagate.${i////}.hdf5"
	    if [ -f "${path}" ]; then
		( set -x
		    "scripts/table-error-propagate.py" "${path}"
		) 2>&1 | tee "$(output-path ${i////} true tex)"
		"mv" $(output-path ${i////} false "tex") "$(final-path true)"
		"mv" $(output-path ${i////} true "tex") "$(final-path true)"
	    else
		echo "${path} not found"
	    fi
	fi
    done
}

function main () {
    echo "${ResultDir}"
    if [ -d "${ResultDir}" ]; then
	dir=${ResultDir%/}
    else
	echo "${ResultDir} no valid dir"
	exit
    fi

    local run="$1"
    shift
    "${run}" $@
}
